# gitlab-messaging-bot

This bot allows you to comment on multiple issues based on certain criterea. It mentions participants directly so they will get a to-do on your comment. It can be used to notify customers directly for upcoming changes or as way to get feedback.

## How it works

The script connects to the GitLab API using the python-gitlab library to add a comment to issues in a GitLab project that match specific label requirements. The script takes input from an input.json file which includes the GitLab project ID, the labels to search for, a list of excluded usernames, and a test issue ID for testing purposes.

The script includes functions for filtering out GitLab users based on their username and state, and for adding a comment to an issue mentioning the participants, except excluded users defined in the input.json file.

The main() function retrieves the list of issues and participants for the project and adds comments to the issues that match the label requirements. The script also includes options for running in dry-run mode without commenting, enabling debug logging, and testing against a test issue with a specific ID.

You can find a walkthrough video [here](https://youtu.be/bTiHzcXj8jY).

## Installation

### Clone the repository locally

Clone the repository locally and open a terminal. You should navigate to `/gitlab-messaging-bot/.`

### Create .env File

```bash
touch .env
vim .env
```

Your `personal_access_token` requires `api` access. Info on how to create gitlab access token can be found [here](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

```bash
GITLAB_TOKEN = 'personal_access_token'
```

Press `ESC` & execute `:wq` to save & exit

### Install Requirements

```bash
pip3 install -r requirements.txt
```

## Usage

### Define parameters in `input.json`

- `projectID` is the ID of the project to which this bot is commenting
- `searchLabels` is a list of labels to search for matching issues
- `excludedUsernames` is a list of usernames to exclude, this most likely includes gitlab users or yourself who you don't want to mention in the comment
- `testIssueIID` is the ID of the test issue to which this bot is commenting when run in test mode `--test`. 

> **NOTE:** The test issue must be created manually before.

### Create message in `message.md`

Create the message you want to send in `message.md` file.
As GitLab comments use markdown syntax you can either specify it in the file directly or write a test message and copy & paste the content to `message.md`.

> **NOTE:**
> - If you copy & paste, do it from within the edit mode. Otherwise the format will not work properly.
> - The message should not include an introduction. The bot will comment with 'Hi @mentions,' + content of `message.md`.

### Authentication

Open a terminal navigate to `/gitlab-messaging-bot/.` and set the credentials from `.env` file for a new session.

```bash
set -a; source .env
```

### Optional Input Arguments

- `-d` or `--debug` will enable verbose debug logging.
- `-dr` or `--dry-run` will execute in dry-run mode and not comment on any issues.
- `-t` or `--test` will execute on a specific test issue TEST_ISSUE_IID.

### Run the script

Perform test first to check if the script will do what you expect.

```bash
python3 -m src -dr -d
```

```bash
python3 -m src -t -d
```

If it does what you want it to do, execute the script with

```bash
python3 -m src
```

### Error Handling

For Error `gitlab.exceptions.GitlabHttpError: 404: 404 Project Not Found` check if you have set environment variables for your session as mentioned [here](#usage).
