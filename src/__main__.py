import os
import sys
import json
import argparse
import gitlab
from src import logger


# Set up GitLab API endpoint and personal access token
GITLAB_URL = 'https://gitlab.com'
GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')

logger = logger.get_logger(__name__)


# Functions
def parse_args(args):
    parser = argparse.ArgumentParser()
    parser.add_argument("-dr", "--dry-run", action="store_true",
                        help="Dry run without commenting")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="Enable debug logging")
    parser.add_argument("-t", "--test", action="store_true",
                        help="Testing against a test issue with TEST_ISSUE_IID")
    return parser.parse_args(args)


# Filter out gitlab users based on username and state
def filter_usernames(participants, EXCLUDED_USERNAMES):
    filtered_participants = ['@' + p['username'] for p in participants
                             if p['username'].casefold() not in EXCLUDED_USERNAMES
                             and p['state'] == 'active']
    logger.debug("Filtered_participants: %s", filtered_participants)
    return filtered_participants


# Add a comment to an issue mentioning the participants, except exlcuded users defined in input.json
def add_comment(args, issue, EXCLUDED_USERNAMES):
    logger.debug("Adding comment to issue: %s", issue.iid)

    # fetch all participants
    participants = issue.participants()
    logger.debug("All participants: %s", participants)

    # filter out gitlab and unactive participants
    filtered_participants = filter_usernames(participants, EXCLUDED_USERNAMES)
    if not filtered_participants:
        logger.debug(
            "Issue iid: %s. No participants left to mention", issue.iid)
        return False  # No participants to mention
    intro = 'Hi ' + ', '.join(filtered_participants) + '\n\n'
    logger.debug("Intro text: %s", intro)

    # read the contents of the Markdown file
    with open('./message.md', 'r') as file:
        message_content = file.read()
    message_body = intro + message_content

    # Add comment to issue when not dry run
    if not args.dry_run:
        message = issue.notes.create({'body': message_body})
        logger.debug("Comment: %s added to issue %s", message, issue.iid)
        return True


# Retrieve list of issues and participants for the project, and add comments
def main():
    """Main."""
    args = parse_args(sys.argv[1:])
    comment_count = 0

    # Load the JSON data from the input.json file to variables
    with open('input.json', 'r') as f:
        input_data = json.load(f)

    PROJECT_ID = input_data['projectID']
    SEARCH_LABELS = input_data['searchLabels']
    EXCLUDED_USERNAMES = [username.casefold()
                          for username in input_data['excludedUsernames']]
    TEST_ISSUE_IID = input_data['testIssueIID']

    if args.dry_run:
        logger.info(
            "********** RUNNING IN DRYRUN MODE - NO CHANGES WILL BE PERSISTED **********")

    if args.test:
        logger.info(
            "********** RUN ON TEST ISSUE TEST_ISSUE_IID **********")

    logger.info("You are running the script with arguments: %s", args.__dict__)

    try:
        # Connect to GitLab API using the python-gitlab library
        with gitlab.Gitlab(GITLAB_URL, GITLAB_TOKEN) as gl:
            logger.debug("Authenticated to gitlab: %s", gl)
            project = gl.projects.get(PROJECT_ID)
            logger.debug("Project: %s", project)

            for issue in project.issues.list(iterator=True):
                logger.debug("Iterating Issue ID: %s", issue)
                # Run on test issue
                if args.test and int(TEST_ISSUE_IID) == issue.iid:
                    logger.info("Adding comment to test issue: %s",
                                TEST_ISSUE_IID)
                    if add_comment(args, issue, EXCLUDED_USERNAMES):
                        comment_count += 1
                    return  # Return after testing

                # Run when not in test mode
                elif not args.test and set(SEARCH_LABELS).issubset(set(issue.labels)):
                    logger.debug("Issue matches label requirements")
                    if add_comment(args, issue, EXCLUDED_USERNAMES):
                        comment_count += 1
    finally:
        logger.info("Commented %s Issue(s)", comment_count)
        if args.dry_run:
            logger.info(
                "********** DRYRUN MODE COMPLETED - NO CHANGES PERSISTED **********")


if __name__ == '__main__':
    main()
