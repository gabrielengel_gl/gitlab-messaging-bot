"""Logger utility class"""
import logging


def get_logger(name):
    """Should be used by all modules to setup their logger object
    """
    logger = logging.getLogger(name)
    formatter = logging.Formatter(
        '%(asctime)s: %(name)s - %(levelname)s: %(message)s')
    logger.setLevel(logging.DEBUG)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(logging.INFO)

    file_handler = logging.FileHandler('debug.log')
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)

    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger
