import unittest
from unittest.mock import MagicMock, patch

from src.__main__ import filter_usernames, add_comment


class TestFilterUsernames(unittest.TestCase):
    def test_filter_usernames(self):
        participants = [{'username': 'user1', 'state': 'active'},
                        {'username': 'user2', 'state': 'inactive'},
                        {'username': 'peter-pan', 'state': 'active'},
                        {'username': 'John', 'state': 'active'},
                        {'username': 'user3', 'state': 'active'}]
        excluded_usernames = ['john', 'peter-pan']
        expected_output = ['@user1', '@user3']
        self.assertEqual(filter_usernames(
            participants, excluded_usernames), expected_output)


class TestAddComment(unittest.TestCase):
    def setUp(self):
        self.args = MagicMock()
        self.issue = MagicMock()
        self.excluded_usernames = MagicMock()

    @patch('src.__main__.logger')
    def test_add_comment_with_no_participants(self, mock_logger):
        self.issue.participants.return_value = []
        add_comment(self.args, self.issue, self.excluded_usernames)
        mock_logger.debug.assert_called_with(
            "Issue iid: %s. No participants left to mention", self.issue.iid)

    @patch('src.__main__.logger')
    def test_add_comment_with_participants(self, mock_logger):
        self.excluded_usernames = MagicMock()
        self.issue.participants.return_value = [
            {'username': 'user1', 'state': 'active'},
            {'username': 'user2', 'state': 'active'}
        ]
        add_comment(self.args, self.issue, self.excluded_usernames)
        mock_logger.debug.assert_called_with(
            'Intro text: %s', 'Hi @user1, @user2\n\n')
